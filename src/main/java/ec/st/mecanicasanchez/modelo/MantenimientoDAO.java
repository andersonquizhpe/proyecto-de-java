package ec.st.mecanicasanchez.modelo;

import com.st.mecanicasanchez.vista.Vista;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Santiago Tuqueres
 */
public class MantenimientoDAO {

    private static final Logger LOG = Logger.getLogger(Conexion.class.getName());
    Conexion conectar = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;

    Vista vista = new Vista();

    public List buscar(String placa) {
        List<Vehiculo> datos = new ArrayList<>();
        String sql = "SELECT* FROM vehiculo WHERE placa LIKE ?";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, placa + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Vehiculo carro = new Vehiculo();

                carro.setPlaca(rs.getString(2));
                carro.setMarca(rs.getString(3));
                carro.setColor(rs.getString(4));
                carro.setModelo(rs.getString(5));
                carro.setAño(rs.getInt(6));
                carro.setChevy(rs.getInt(7));
                carro.setCorreo(rs.getString(8));
                carro.setCarroid(rs.getInt(1));

                datos.add(carro);
                //JOptionPane.showMessageDialog(vista, "Busqueda Exitosa");
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(vista, "No se encontraron resultados");
        }
        return datos;
    }

    public List buscar_mantenimiento(String placa) {
        List<Mantenimiento> datos = new ArrayList<>();
        String sql = "select mantenimiento_id, fecha, kilometraje, servicio, observaciones from mantenimiento m INNER JOIN vehiculo v  ON m.carro_id = v.carro_id  where v.placa LIKE ?";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, placa + "%");
            rs = ps.executeQuery();

            while (rs.next()) {

                Mantenimiento m = new Mantenimiento();
                Vehiculo v = new Vehiculo();

                m.setMantenimiento_id(rs.getInt(1));
                m.setFecha_revision(rs.getString(2));
                m.setKilometraje(rs.getInt(3));
                m.setServicio(rs.getString(4));
                m.setObservaciones(rs.getString(5));

                datos.add(m);

            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(vista, "No se encontraron resultados");
        }
        return datos;
    }

    public int agregar(Mantenimiento m) {
        String sql = "insert into mantenimiento( fecha,kilometraje, servicio, observaciones, carro_id) values (curdate(),?,?,?,?)";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);

            ps.setInt(1, m.getKilometraje());
            ps.setString(2, m.getServicio());
            ps.setString(3, m.getObservaciones());
            ps.setInt(4, m.getCarro_id());

            ps.executeUpdate();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
        return 1;
    }

    public void eliminar(int id) {
        String sql = "delete from mantenimiento where mantenimiento_id=" + id;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
    }
}

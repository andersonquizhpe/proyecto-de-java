package ec.st.mecanicasanchez.modelo;

import com.st.mecanicasanchez.vista.Vista;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author SANTIAGO 1998
 */
public class ClienteDAO {

    private static final Logger LOG = Logger.getLogger(Conexion.class.getName());
    Conexion conectar = new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Vista vista = new Vista();
    
    public int actualizar(Cliente c) {
        String sql = "update cliente set nombre=?, celular=? where cliente_id=?";
        int r = 0;
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, c.getNombre());
            ps.setInt(2, c.getCelular());
            ps.setInt(3, c.getCliente_id());
            
            r = ps.executeUpdate();
            if (r == 1) {
                return 1;
            } else {
                return 0;
            }
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
        return r;
    }

    public int agregar(Cliente c) {
        String sql = "INSERT INTO cliente(nombre, celular, carro_id) VALUES (?,?,(SELECT max(carro_id) FROM vehiculo))";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, c.getNombre());
            ps.setInt(2, c.getCelular());

            ps.executeUpdate();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, null, e);
        }
        return 1;
    }

    public List buscar(String placa) {
        List<Cliente> datos = new ArrayList<>();
        String sql = "SELECT placa,marca, modelo, color, año, chevy, correo, nombre, celular, c.cliente_id, c.carro_id from vehiculo v INNER JOIN cliente c ON c.carro_id = v.carro_id where v.placa LIKE ?";
        try {
            con = conectar.getConnection();
            ps = con.prepareStatement(sql);
            ps.setString(1, placa + "%");
            rs = ps.executeQuery();
            while (rs.next()) {

                Cliente cliente = new Cliente();
                cliente.setCliente_id(rs.getInt(10));
                cliente.setNombre(rs.getString(8));
                cliente.setCelular(rs.getInt(9));
                datos.add(cliente);
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(vista, "No se encontraron resultados");
        }
        return datos;
    }
}

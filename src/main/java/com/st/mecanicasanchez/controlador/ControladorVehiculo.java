package com.st.mecanicasanchez.controlador;

import com.st.mecanicasanchez.vista.Vista;
import com.st.mecanicasanchez.vista.VistaConsultasDia;
import com.st.mecanicasanchez.vista.VistaVehiculoModificar;
import ec.st.mecanicasanchez.modelo.Cliente;
import ec.st.mecanicasanchez.modelo.ClienteDAO;
import ec.st.mecanicasanchez.modelo.Conexion;
import ec.st.mecanicasanchez.modelo.Vehiculo;
import ec.st.mecanicasanchez.modelo.VehiculoDAO;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Santiago Tuqueres
 */
public class ControladorVehiculo implements ActionListener {

    private static final Logger LOG = Logger.getLogger(ControladorVehiculo.class.getName());
    VehiculoDAO dao = new VehiculoDAO();
    Vehiculo carro = new Vehiculo();

    ClienteDAO cliente_dao = new ClienteDAO();
    Cliente cliente = new Cliente();
    Vista vista = new Vista();
    VistaVehiculoModificar vista2 = new VistaVehiculoModificar();
    VistaConsultasDia vista3 = new VistaConsultasDia();

    public ControladorVehiculo(Vista v) {
        this.vista = v;
        //this.vista.btn_listar.addActionListener(this);
        this.vista.btn_guardar.addActionListener(this);
        //this.vista.btn_editar.addActionListener(this);
    }
    DefaultTableModel modelo = new DefaultTableModel();

    public ControladorVehiculo(VistaVehiculoModificar v) {
        this.vista2 = v;
        //this.vista2.btn_listar.addActionListener(this);
        this.vista2.btn_guardar.addActionListener(this);
        this.vista2.btn_editar.addActionListener(this);
    }

    public ControladorVehiculo(VistaConsultasDia v) {
        this.vista3 = v;
    }

    public ControladorVehiculo() {
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
    }

    public void actualizar(int id, String placa, String marca, String modelo, String color, int año, int chevy, String correo) {
        carro.setCarroid(id);
        carro.setPlaca(placa);
        carro.setMarca(marca);
        carro.setModelo(modelo);
        carro.setColor(color);
        carro.setAño(año);
        carro.setChevy(chevy);
        carro.setCorreo(correo);
        int r = dao.actualizar(carro);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista, "Vehiculo Actualizado con exito");
            //limpiarTabla();
        } else {
            JOptionPane.showMessageDialog(vista, "Error al modificar");
        }
    }

    public void actualizar_cliente(int cliente_id, String nombre, int celular) {
        cliente.setCliente_id(cliente_id);
        cliente.setNombre(nombre);
        cliente.setCelular(celular);
        int r = cliente_dao.actualizar(cliente);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista, "Cliente Actualizado con exito");
            //limpiarTabla();
        } else {
            JOptionPane.showMessageDialog(vista, "Error al modificar");
        }
    }

    public void agregar(String placa, String marca, String color, String modelo, int año, int chevy, String correo, String nombre_cliente, int celular) {
        carro.setPlaca(placa);
        carro.setMarca(marca);
        carro.setColor(color);
        carro.setAño(año);
        carro.setChevy(chevy);
        carro.setCorreo(correo);
        carro.setModelo(modelo);
        int r = dao.agregar(carro);
        if (r == 1) {
            JOptionPane.showMessageDialog(vista, "Vehiculo Agregado con Exito");
            //limpiarTabla();
        } else {
            JOptionPane.showMessageDialog(vista, "Hubo un Error");
        }

        cliente.setNombre(nombre_cliente);
        cliente.setCelular(celular);

        int r2 = cliente_dao.agregar(cliente);
        if (r2 == 1) {
            //JOptionPane.showMessageDialog(vista, "Cliente Agregado con Exito");
            //limpiarTabla();
        } else {
            //JOptionPane.showMessageDialog(vista, "Hubo un Error");
        }
    }

    public void listar_buscar(JTable tabla, String criterio) {
        modelo = (DefaultTableModel) tabla.getModel();
        List<Vehiculo> lista = dao.buscar(criterio);
        List<Cliente> lista2 = cliente_dao.buscar(criterio);
        Object[] object = new Object[11];
        for (int i = 0; i < lista.size(); i++) {
            object[0] = lista.get(i).getCarroid();
            object[1] = lista.get(i).getPlaca();
            object[2] = lista.get(i).getMarca();
            object[3] = lista.get(i).getModelo();
            object[4] = lista.get(i).getColor();
            object[5] = lista.get(i).getAño();
            object[6] = lista.get(i).getChevy();
            object[7] = lista.get(i).getCorreo();
            object[8] = lista2.get(i).getCliente_id();
            object[9] = lista2.get(i).getNombre();
            object[10] = lista2.get(i).getCelular();
            modelo.addRow(object);
        }
        if (object != null) {
            //JOptionPane.showMessageDialog(vista2, "Busqueda Exitosa");
        } else {
            JOptionPane.showMessageDialog(vista2, "No se ha encontrado resultados en la Busqueda");
        }
        //vista.tabla.setModel(modelo);
    }

    public void listar(JTable tabla) {
//        centrarCeldas(tabla);
        modelo = (DefaultTableModel) tabla.getModel();
        List<Vehiculo> lista = dao.listar();
        Object[] object = new Object[7];
        for (int i = 0; i < lista.size(); i++) {
            //object[0] = lista.get(i).getCarroid();
            object[0] = lista.get(i).getPlaca();
            object[1] = lista.get(i).getMarca();
            object[2] = lista.get(i).getColor();
            object[3] = lista.get(i).getAño();
            object[4] = lista.get(i).getChevy();
            object[5] = lista.get(i).getCorreo();
            object[6] = lista.get(i).getModelo();
            modelo.addRow(object);
        }

    }

    public void consulta_diaria(JTable tabla, String fecha) {
        modelo = (DefaultTableModel) tabla.getModel();
        List<Vehiculo> lista = dao.consultar_diario(fecha);
        Object[] object = new Object[4];
        for (int i = 0; i < lista.size(); i++) {
            //object[0] = lista.get(i).getCarroid();
            object[0] = lista.get(i).getMarca();
            object[3] = lista.get(i).getColor();
            object[2] = lista.get(i).getAño();
            object[1] = lista.get(i).getModelo();
            modelo.addRow(object);
        }
    }
    public Boolean esRepetido(String placa) {
        Boolean estado = dao.verificarVehiculo(placa);
        System.out.println("repetifo "+estado);
        return estado;
    }
}
